require 'toto'
require 'rack/rewrite'
require 'rack/coderay'
require 'haml'
require './lib/famous_quotes'
require './lib/archive_helper'
require './lib/tag'

# Rack config
use Rack::Static, :urls => [
  '/stylesheets', 
  '/javascripts',
  '/images', 
  '/resources',
  '/favicon.ico', 
  '/robots.txt', 
  '/sitemap.xml', 
  /\/*.html/
], :root => 'public'

use Rack::CommonLogger
use Rack::Rewrite do
  r301 '/2010/04/installing-vim-rails-on-opensuse-112.html', '/2010/04/18/installing-vim-rails-on-opensuse-112'
  r301 '/2011/03/rain-barrel.html', '/2011/03/20/rain-barrel'
  r301 '/2011/04/flog-hates-mongoid.html', '/2011/04/19/flog-hates-ruby-19'
  r301 '/p/about.html', '/about'
  r301 '/2009/10/31/krav-maga-for-self-defense', '/2009/10/krav-maga-for-self-defense'
  r301 '/2011/12/28/get-your-ccw-license-while-their-available/', 'http://www.preppingtips.com/2011/12/28/get-your-ccw-license-while-their-available/'
  r301 '/2011/12/28/avoid-winter-weather-driving/', 'http://www.preppingtips.com/2011/12/28/avoid-winter-weather-driving/'

  if ENV['RACK_ENV'] == 'production'
    r301 %r(.*), 'http://www.codingtofreedom.com$&', :if => Proc.new {|rack_env|
      rack_env['SERVER_NAME'] != 'www.codingtofreedom.com'
    }
  end
end
use Rack::Coderay

if ENV['RACK_ENV'] == 'development'
  use Rack::ShowExceptions
end

#
# Create and configure a toto instance
#
toto = Toto::Server.new do
  set :to_html,     lambda {|path, page, ctx|
    ::Haml::Engine.new(File.read("#{path}/#{page}.haml"), :format => :html5, :ugly => true).render(ctx)
  }
  #set :error do |code|
  #  ::Haml::Engine.new(File.read("templates/pages/#{code}.haml"), :format => :html5, :ugly => true).render(@context)
  #end
  # Add your settings here
  # set [:setting], [value]
  # 
  set :author,      'Sean Kibler'    
  set :title,       'Coding To Freedom (One line at a time)' 
  # set :root,      "index"                                   # page to load on /
  # set :date,      lambda {|now| now.strftime("%d/%m/%Y") }  # date format for articles
  # set :markdown,  :smart                                    # use markdown + smart-mode
  set :disqus,    'codingtofreedom'                           # disqus id, or false
  # set :summary,   :max => 150, :delim => /~/                # length of article summary and delimiter
  # set :ext,       'txt'                                     # file extension for articles
  # set :cache,      28800                                    # cache duration, in seconds
  set :url,         ENV['RACK_ENV'] == 'development' ? 'http://codingtofreedom.dev' : 'http://www.codingtofreedom.com'
  set :date, lambda {|now| now.strftime("%B #{now.day.ordinal} %Y") }
  set :error, lambda {|code|
    File.read("public/#{code}.html") 
  }
  set :famous_quote,     FAMOUS_QUOTES[rand(4)].values.to_a
end

run toto
