require 'nokogiri'
require './lib/blogger_article'

class BloggerImporter
  def initialize(blog_id)
    @blog_id = blog_id
    @blog_data = Nokogiri::HTML(open("http://www.blogger.com/feeds/#{@blog_id}/posts/default"))
  end

  def articles
    @blog_data.css('entry').collect {|article|
      BloggerArticle.new(
        :title => article.at_css('title').inner_text, 
        :published => article.at_css('published').inner_text, 
        :content => article.at_css('content').inner_text
      )
    }
  end
end
