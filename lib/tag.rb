class Tag
  def self.list(articles)
    articles.map {|article| 
      article[:tags].split(', ')
    }.flatten.uniq
  end
end
