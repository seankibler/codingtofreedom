require 'action_view'
class BloggerArticle
  include ActionView::Helpers::SanitizeHelper

  attr_accessor :title, :published, :content

  def initialize(attrs={})
    attrs[:published] ||= Time.now
    @title = attrs[:title]
    @published = Time.parse(attrs[:published])
    @content = strip_tags(attrs[:content])
  end
end
