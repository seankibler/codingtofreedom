FAMOUS_QUOTES = [
  {:body => 'Those who would give up Essential Liberty to purchase a little Temporary Safety, deserve neither Liberty nor Safety.', :source => 'Benjamin Franklin'},
  {:body => 'A free people [claim] their rights as derived from the laws of nature, and not as the gift of their chief magistrate.', :source => 'Thomas Jefferson'},
  {:body => 'Keep your eyes wide open before marriage, half shut afterwards. ', :source => 'Benjamin Franklin'},
  {:body => 'Remember, that Time is Money.', :source => 'Benjamin Franklin'}
]
