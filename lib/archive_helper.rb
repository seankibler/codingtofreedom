class ArchiveHelper
  def initialize(archive_path)
    @year, @month = archive_path.split('/')
  end

  def year
    @year 
  end

  def month
    @month if @month
  end

  def month_or_year
    @month ? month_name : @year
  end

  def month_name
    Date::MONTHNAMES[@month.to_i]
  end
end
